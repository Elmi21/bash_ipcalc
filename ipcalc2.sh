 #!/bin/bash
part_translation_number_10_2(){
local num10=`eval "expr \"\$"$1"\" "`; 
local num2=`eval "expr \"\$"$2"\" "`; 
local num2="";
num2=`echo "obase=2; $num10"|bc`;
local length_str=${#num2};
if [ "$length_str" -ne 8 ]
  then
  add_num_characters=$((8-$length_str));
  while [ $add_num_characters -ne 0 ]
  do
    num2="0$num2";
    add_num_characters=$(($add_num_characters-1));
  done
fi
eval "$2=\"$num2\"";
}
translation_number_10_2(){
local num_in=`eval "expr \"\$"$1"\""`;
num_in=( `echo "$num_in"` ); 
declare -a num_out;
for (( i=0; i<4; i++))
do
  part_translation_number_10_2 {num_in[$i]} num_out[$i]
done
eval "$2=( ${num_out[@]} )"; 
}
output(){
local num_in10=`eval "expr \"\$"$1"\""`;
num_in10=( `echo "$num_in10"` ); 
local num_in2=`eval "expr \"\$"$2"\""`;
num_in2=( `echo "$num_in2"` );
local str_in=$3;
echo -e "$str_in:\t"${num_in10[0]}"."${num_in10[1]}"."${num_in10[2]}"."${num_in10[3]}"\t"\
${num_in2[0]}"."${num_in2[1]}"."${num_in2[2]}"."${num_in2[3]};
}
translation_number_2_10(){
local num_in=`eval "expr \"\$"$1"\""`;
num_in=( `echo "$num_in"` ); 
declare -a num_out;
for (( i=0; i<4; i++))
do
  num_out[$i]=`echo "obase=10; ibase=2; ${num_in[$i]}"|bc`;
done
eval "$2=( ${num_out[@]} )";      
}
add_points(){
local str_input=`eval "expr \"\$"$1"\""`;
str_input=${str_input::8}.${str_input:8:8}.${str_input:16:8}.${str_input:24};
eval "$1=\"$str_input\"";
}
str_work(){
local str_local=`eval "expr \"\$"$1"\""`;
add_points str_local;
local str_name=$2;
local num1;
local num2;
num1=( `echo "$str_local"` ); 
translation_number_2_10  {num1[*]} num2;
output {num2[*]} {num1[*]} $str_name;
eval "$1=\"$str_local\"";
}
#Address
IFSorigin=$IFS;
IFS=$'.'$'/';
num=( `echo "$1"` ); 
translation_number_10_2  {num[*]} num_ip_address
for (( i=0; i<4; i++))
do
  str_ip_address="$str_ip_address""${num_ip_address[$i]}";
done
output {num[*]} {num_ip_address[*]} 'Address';
#Netmask
mask=${num[4]};
str_mask="";
for (( i=1; i<=$mask; i++))
do
  str_mask="1$str_mask";
done
num_zero=$((32-$mask));
for (( i=1; i<=$num_zero; i++))
do
  str_mask="$str_mask"'0';
done
str_work str_mask 'Netmask' 
#Wildcard
str_wildcard="";
for (( i=1; i<=$mask; i++))
do
  str_wildcard='0'"$str_wildcard";
done
for (( i=1; i<=$num_zero; i++))
do
  str_wildcard="$str_wildcard"'1';
done
str_work str_wildcard 'Wildcard' 
#Network
num_zero_str="";
for (( i=1; i<=$num_zero; i++))
do
  num_zero_str='0'$num_zero_str;
done
str_network=`echo $str_ip_address|sed "s/[0-9]\{$num_zero\}$/$num_zero_str/"`;
str_work str_network 'Network' 
#Broadcast
num_one_str="";
for (( i=1; i<=$num_zero; i++))
do
  num_one_str='1'$num_one_str;
done
str_broadcast=`echo $str_ip_address|sed "s/[0-9]\{$num_zero\}$/$num_one_str/"`;
str_work str_broadcast 'Broadcast' 
#Hostmin
str_hostmin=`echo $str_network|sed "s/[0-9]\{1\}$/1/"`;
IFS=$'.';
num_hostmin=( `echo "$str_hostmin"` ); 
IFS=$IFSorigin;
translation_number_2_10  {num_hostmin[*]} num;
output {num[*]} {num_hostmin[*]} 'Hostmin';
#Hostmax
str_hostmax=`echo $str_broadcast|sed "s/[0-9]\{1\}$/0/"`;
IFS=$'.';
num_hostmax=( `echo "$str_hostmax"` ); 
IFS=$IFSorigin;
translation_number_2_10  {num_hostmax[*]} num;
output {num[*]} {num_hostmax[*]} 'Hostmax';
let "hosts=2**$num_zero-2";
echo -e "Hosts:\t\t"$hosts;
